let burritochanger = [
    "https://goldbelly.imgix.net//uploads/showcase_media_asset/image/96814/hot-link-breakfast-burrito.dca3069bdb4ff0b17b65da9a61cc6d27.jpg",
    "https://cdn.vox-cdn.com/thumbor/OYK9VFTCxn0okIhTzeHUQ-78CXo=/0x0:2500x2000/1820x1213/filters:focal(1050x800:1450x1200):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/71819400/bacon_burrito_cofax.0.jpg",
    "https://images.squarespace-cdn.com/content/v1/58a22d31bf629a87d276c5fd/1591345752672-A6ONQ1NPNU7HKU1FHA6V/Burrito.jpg",
    "https://www.thevegspace.co.uk/wp-content/uploads/2017/01/chilli-burrito-680x879.jpg",
    "https://cdn.britannica.com/13/234013-050-73781543/rice-and-chorizo-burrito.jpg",
    "https://s.hdnux.com/photos/01/27/20/66/22875571/4/rawImage.jpg",
    "https://images.themodernproper.com/billowy-turkey/production/posts/2019/breakfast-burrito-6.jpg?w=1200&h=1800&q=82&fm=jpg&fit=crop&dm=1599768224&s=a8770a8102ce9a050ee8df02b4fce2f5",
    "https://carlsbadcravings.com/wp-content/uploads/2022/04/Beef-Burritos-6.jpg",
  ];
  
  const imgs = document.getElementsByTagName("img");
  
  for (image of imgs) {
    const index = Math.floor(Math.random() * burritochanger.length);
    image.src = burritochanger[index];
  }